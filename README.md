# Accord Compose

This is the easy way to get a local version up and running.

# Usage

1. Install [Docker](https://www.docker.com/get-started/) and [Docker Compose](https://docs.docker.com/compose/install/)
1. Generate a random sequene of characters as your secret
1. run `cp .env.example .env` open `.env` and fill it out

## Development

1. Run `./dev-compose.sh up`
1. Go to [localhost:8080](http://localhost:8080)

## Production

1. Start Traefik using their [quickstart config](https://docs.traefik.io/#the-traefik-quickstart-using-docker)
1. Create a `.env` file with `DOMAIN=[SUB.YOUR_DOMAIN]`
1. Run `./prod-compose up`
1. Visit [SUB.YOUR_DOMAIN]
